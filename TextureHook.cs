﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.FoxCore;
using NimbusFox.FoxCore.Classes;
using NimbusFox.OverrideAPI.Patches;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Core;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Tiles;

namespace NimbusFox.TexturePackMod {
    public class TextureHook : IModHookV3 {
        internal static IReadOnlyDictionary<string, string> Tiles => _tiles;
        internal static IReadOnlyDictionary<string, string> Items => _items;

        private static Dictionary<string, string> _tiles = new Dictionary<string, string>();
        private static Dictionary<string, string> _items = new Dictionary<string, string>();

        internal string CurrentPack = "Vanilla";

        internal readonly Fox_Core FoxCore;

        internal static TextureHook Instance;

        public TextureHook() {
            FoxCore = new Fox_Core("NimbusFox", "TexturePackMod", "0.1");

            if (!FoxCore.ConfigDirectory.FileExists("config.json")) {
                using (var ms = new MemoryStream()) {
                    var blob = BlobAllocator.Blob(true);

                    blob.SetString("pack", CurrentPack);

                    blob.SaveJsonStream(ms);

                    FoxCore.ConfigDirectory.WriteFileStream("config.json", ms, true);

                    Blob.Deallocate(ref blob);
                }
            } else {
                var blob = FoxCore.ConfigDirectory.ReadFile<Blob>("config.json", true);

                CurrentPack = blob.GetString("pack", "Vanilla");

                Blob.Deallocate(ref blob);
            }

            Instance = this;

            FoxCore.PatchController.Override(typeof(TileDatabase), "LoadDefinitions", typeof(TileDatabasePatches),
                nameof(TileDatabasePatches.LoadDefinitionsInit), TileDatabasePatches.LoadDefinitionsTranspiler);
        }

        internal static void Reload() {
            _tiles.Clear();
            _items.Clear();

            if (Instance.CurrentPack == "Vanilla") {
                return;
            }

            var packs = GameContext.AssetBundleManager.FindByExtension(".txpack");

            Blob pack = null;

            foreach (var path in packs) {
                try {
                    var blob = GameContext.Resources.FetchBlob(path);

                    if (blob.Contains("name")) {
                        if (blob.GetString("name") == Instance.CurrentPack) {
                            pack = blob;
                            break;
                        }
                    }

                    Blob.Deallocate(ref blob);
                } catch {
                    // catch non blob errors
                }
            }

            if (pack == null) {
                Instance.CurrentPack = "Vanilla";
                return;
            }

            if (pack.Contains("tiles")) {
                _tiles = pack.GetObject<Dictionary<string, string>>("tiles");
            }

            if (pack.Contains("items")) {
                _items = pack.GetObject<Dictionary<string, string>>("items");
            }
        }

        public void Dispose() { }
        public void GameContextInitializeInit() {}

        public void GameContextInitializeBefore() {
        }

        public void GameContextInitializeAfter() {
        }
        public void GameContextDeinitialize() { }

        public void GameContextReloadBefore() {
        }
        public void GameContextReloadAfter() {
        }
        public void UniverseUpdateBefore(Universe universe, Timestep step) { }
        public void UniverseUpdateAfter() { }
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return true;
        }

        public void ClientContextInitializeInit() { }
        public void ClientContextInitializeBefore() { }
        public void ClientContextInitializeAfter() { }
        public void ClientContextDeinitialize() { }
        public void ClientContextReloadBefore() { }
        public void ClientContextReloadAfter() {
        }
        public void CleanupOldSession() { }
        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return true;
        }

        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }
    }
}
